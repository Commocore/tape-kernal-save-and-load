
REM Prepare empty TAP file
copy empty-tape.tap build\tape.tap

REM Compile save and load programs
"tools\64tass.exe" -C -a -B tape-save-kernal.asm -o "build\tape-save-kernal.prg" || exit /b
"tools\64tass.exe" -C -a -B tape-load-kernal.asm -o "build\tape-load-kernal.prg" || exit /b

REM Execute first program to save data to a tape
"C:\Program Files\WinVice-2.4-x64\x64.exe" ^
	-1 build\tape.tap ^
	-autostartprgmode 0 ^
	-autostart build\tape-save-kernal.prg ^
	-remotemonitor
	
REM Now, let's try to load data from a tape with the second program!
"C:\Program Files\WinVice-2.4-x64\x64.exe" ^
	-1 build\tape.tap ^
	-autostartprgmode 0 ^
	-autostart build\tape-load-kernal.prg ^
	-remotemonitor
	