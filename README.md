﻿
# Saving and loading files from tape using KERNAL

Example of saving and loading a file from a tape using Commodore 64 KERNAL.
Source code has been compiled with 64tass cross-assembler.

Run `execute.bat` to compile both programs and execute one after another with WinVice (you have to install WinVice).
File will be saved to `build/tape.tap` folder.
