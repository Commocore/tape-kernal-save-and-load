﻿
; @access public
; @uses word FILENAME
; @return void
loadFromTape .proc
	
	; Set device
	lda #FILE_LOGICAL_NUMBER ; logical number
	ldx #TAPE_DEVICE_NUMBER ; tape device number
	ldy #TAPE_RELOCATABLE_SECONDARY_ADDRESS ; secondary address
	jsr KERNAL_SETLFS
	
	; Set filename
	lda #size(FILENAME) ; file name length
	ldx #<FILENAME ; pointer to filename
	ldy #>FILENAME ; pointer to filename
	jsr KERNAL_SETNAM
	
	; Open file
	lda #0 ; load option
	ldx #<SCREEN
	ldy #>SCREEN
	jsr KERNAL_LOAD
	bcs loadError
		; Success!
		lda #5
		sta $d020
rts


; @access private
; @return void
loadError
	; Failed...
	lda #2
	sta $d020
rts

.pend
