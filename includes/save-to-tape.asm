﻿
; @access public
; @uses word FILENAME
; @return void
saveToTape .proc
	
	; Set filename
	lda #size(FILENAME) ; file name length
	ldx #<FILENAME ; pointer to filename
	ldy #>FILENAME ; pointer to filename
	jsr KERNAL_SETNAM
	
	; Set device
	lda #FILE_LOGICAL_NUMBER ; logical number
	ldx #TAPE_DEVICE_NUMBER ; tape device number
	ldy #TAPE_RELOCATABLE_SECONDARY_ADDRESS ; secondary address
	jsr KERNAL_SETLFS
	
	; Save data
	lda #<DATA_LOCATION
	sta tempWord + 0
	lda #>DATA_LOCATION
	sta tempWord + 1
	
	ldx #<EOF_DATA_LOCATION
	ldy #>EOF_DATA_LOCATION
	lda #tempWord
	jsr KERNAL_SAVE
	bcs saveError
		; Success!
		lda #5
		sta $d020
rts


; @access private
; @return void
saveError
	; Failed...
	lda #2
	sta $d020
rts

.pend
